@extends('layouts.master')
@section('title' , $post->title)
@section('content')

    <h2 class="text-center">{{$post->title}} </h2>

    <p class="text-center">{{$post->description}}</p>

    <h5 class="text-center">writer : {{$post->user->name}}</h5>
@endsection
