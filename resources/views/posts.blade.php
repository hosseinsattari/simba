@extends('layouts.master')
@section('title' , 'articles')
@section('content')

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">post title</th>
            <th scope="col">writer</th>
        </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
            <tr>
                <td>{{$loop->index}}</td>
                <td><a href="{{route('single' , ['post' => $post->id])}}">{{$post->title}}</a></td>
                <td>{{$post->user->name . $post->user->last_name}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
