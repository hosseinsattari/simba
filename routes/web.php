<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/users', function () {
    return view('users' , [
        'users' => \App\Models\User::all(),
    ]);
});

Route::get('/posts', function () {
    return view('posts' , [
        'posts' => \App\Models\post::all(),
    ]);
});

Route::get('/post/{post}', function () {
    return view('single' , [
        'post' => \App\Models\post::findorfail(request()->post),
    ]);
})->name('single');

