<?php

namespace Database\Seeders;

use App\Models\post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::factory(5)->create()->each(function ($user){
           post::factory(rand(3,5))->make()->each(function ($post) use ($user){
             $post->user_id = $user->id;
             $post->save();
           });
        });
    }
}
